package com.orghrm.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.orghrm.pages.HomePage;
import com.orghrm.pages.LoginPage;

public class HomeTest extends BaseTest {	
	LoginPage lp;
	HomePage hp;
	
	public HomeTest() {
		super();
	}
	
	@BeforeClass
	public void setUp() {
		lp=new LoginPage(driver);
	}
	
	@Test(priority=1)
	public void verifyValidLogin() {
		String validUser=config.getLoginUserName(); //Data source -> config.properties
		String validPassword=config.getLoginPassword(); //Data source -> config.properties
		String expectedWelcomeText="Welcome Admin";
	
		hp = lp.doLogin(validUser, validPassword);
		String actualWelcomeText = hp.getHomePageHeader();
		Assert.assertEquals(actualWelcomeText, expectedWelcomeText);
	}
	
	@Test(priority=2)
	public void verifyHomePageTitle() {
		String expectedHomepageTitle="OrangeHRM";
		String actualPageTitle = hp.getHomePageTitle();
		Assert.assertEquals(actualPageTitle, expectedHomepageTitle);
	}

	@Test(priority=3)
	public void verifyHomePageHeader() {
		String expectedWelcomeText="Welcome Admin";
		String actualPageHeader = hp.getHomePageHeader();
		Assert.assertEquals(actualPageHeader, expectedWelcomeText);
	}

	@Test(priority=4)
	public void verifyLogout() {
		String expectedLoginPageHeader="LOGIN Panel";
		lp = hp.doLogOut();
		String actualPageHeader=  lp.getLoginPageHeader();
		Assert.assertEquals(actualPageHeader, expectedLoginPageHeader);
	}
}