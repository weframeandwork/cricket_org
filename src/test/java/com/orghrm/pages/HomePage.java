package com.orghrm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {
	//HomePage Locators
	private By homePageHeader = By.cssSelector("#welcome");
	private By welcomeLink = By.xpath("//a[contains(text(),'Welcome')]");
	private By logOutLink = By.partialLinkText("Logout");
	
	public HomePage(WebDriver driver) {
		super(driver);
	}

	
	public WebElement getWelcomeLink() {
		return getElement(welcomeLink);
	}

	public WebElement getLogOutLink() {
		return getElement(logOutLink);
	}
	
	// page Title
	public String getHomePageTitle() {
		return getPageTitle();
	}
	
	//page Header
	public String getHomePageHeader() {
		return getPageHeader(homePageHeader);
	}

	//Logout Functionality
	public LoginPage doLogOut() {
		getWelcomeLink().click();
		getLogOutLink().click();
		return getInstance(LoginPage.class);
	}
	
	//logout page URL
	public String getLogoutPageURL() {
		return getPageURL();
	}
}